# gitlab.com_vs_github.com

A feature comparison of both sites

:+1: Gitlab

* A register of docker images for every repository. 
* Posibility to stage PRs for auto merging upon certain conditions (CI green, approved review, etc).
* Posibility of automatically removing branches after they are merged.
* Force-pushes to branches are handled better, with very clear visualization of what has changed between each push. 
* Code coverage is autodetected by parsing CI logs in search for a custom regexp. 
* Builtin CI/CD support with a similar syntax to CircleCI, but with a better UI. 
* Builtin support for preventing WIP PRs from being merged, no need to install a separate application.
* A "rebase" UI button.
* A time-tracking built-in feature.

:+1: Github (+ CircleCI)

* A nice "blame" view UI (I don't say "better" because Gitlab does not seem to one).
* A nice "conflict resolution" UI (I don't say better because Gitlab does not seem to have one).
* A better "commit per commit" review UI.
* Transfer repos to other users
* CircleCI is more stable than GitlabCI.